var promise = $.ajax({
  type: 'get',
  url: 'https://www.reddit.com/r/javascript.json'
});

promise.then(function(response) {
  var threads = response.data.children;
  var html = '';

  threads.forEach(function(thread) {
    html += '<h3>' + thread.data.title + '</h3>';
  });

  $('#reddit').html(html);
  console.log('response', threads);
});